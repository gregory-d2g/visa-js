# About
This library is an improvement on the Peter Torelli original ni-visa project. 

This project has an object oriented aproach for visa instrumentation.

The library is [available](https://www.ni.com/visa/) from National Instruments for Windows and macOS, and supports GPIB, USB, Serial, Ethernet, VXI and PXI interfaces.

Created by Gregory F. Gusberti - All Electronics Channel
https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug


# Needs
[as admin!!] npm install --global --production windows-build-tools

# Wrapping
This implementation uses `ffi` to export wrapper functions.

# TODO
1. Implement error handling on the object oriented VisaDevice interface
2. Error handling - provide an interface for a custom error handler
3. Solve the FFI DynamicLibrary issue (it appends the .dylib library suffix which breaks on recent macOS)
4. Implement the full v19 API, not just the basic functions

# Thanks
Originally inspired by Peter Torelli     [ni-visa project] (https://github.com/petertorelli/ni-visa).
Originally inspired by Jürgen Skrotzky's [visa32 project]  (https://github.com/Jorgen-VikingGod/visa32).
