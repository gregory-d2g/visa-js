// All Electronics Channel - Gregory F. Gusberti @ 2020
// https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug

const VisaDevice = require('./visa/VisaDevice.js');

const HP5316A = new VisaDevice('TCPIP0::192.168.15.50::gpib0,7::INSTR');

HP5316A.set_termination_char('\r');
HP5316A.write('F1,');
HP5316A.write('WD1,');

setInterval(() =>
{
	console.log(HP5316A.read()[1]);
}, 1000);