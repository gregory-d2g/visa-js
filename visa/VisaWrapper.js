// All Electronics Channel - Gregory F. Gusberti @ 2020
// https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug

const ffi   = require('ffi');
const ref   = require('ref');

const vcon     = require('./constants.js');
const types    = require('./types.js');
const lib_name = require('./lib_name.js');


// 'string' is used to reduce code, the FFI module will create Buffers as needed
const libVisa = ffi.Library(lib_name(), 
{
	// Resource Manager statics and Operations
	'viOpenDefaultRM': [types.ViStatus, [types.ViPSession]],
	'viOpen':          [types.ViStatus, [types.ViSession,  'string', types.ViAccessMode, types.ViUInt32, types.ViPSession]],

	// Resource Template Operations
	'viClose': [types.ViStatus, [types.ViObject]],

	// Config
	'viSetAttribute': [types.ViStatus, [types.ViObject, types.ViAttr, types.ViAttrState]],

	// Basic I/O Operations
	'viRead':       [types.ViStatus, [types.ViSession, types.ViPBuf, types.ViUInt32, types.ViPUInt32]],
	'viReadToFile': [types.ViStatus, [types.ViSession, 'string',     types.ViUInt32, types.ViPUInt32]],
	'viWrite':      [types.ViStatus, [types.ViSession, 'string',     types.ViUInt32, types.ViPUInt32]],
});

// TODO: since error handling is undecided, every static calls this
function statusCheck (status) {
	if (status & vcon.VI_ERROR) {
		console.log('Warning: VISA Error: 0x' + (status >>> 0).toString(16).toUpperCase());
		throw new Error();
	} else {
		if (status) {
			let str = vcon.decodeStatus(status);
			if (str != null) {
				console.log(`non-error status check: ${status.toString(16)} ${str}`);
			} else {
				console.log(`non-error status check: ${status.toString(16)}`);
			}
		}
	}
}

class VisaWrapper
{
	static open_default_rm() 
	{
		const rm_session  = ref.alloc(types.ViSession);
		const status      = libVisa.viOpenDefaultRM(rm_session);

		statusCheck(status);
		return [status, rm_session.deref()];
	}
	
	/*static viFindRsrc (sesn, expr) {
		let status;
		let pFindList = ref.alloc(ViFindList);
		let pRetcnt = ref.alloc(types.ViUInt32);
		let instrDesc = Buffer.alloc(512);
		status = libVisa.viFindRsrc(sesn, expr, pFindList, pRetcnt, instrDesc);
		statusCheck(status);
		return [
			status,
			pFindList.deref(),
			pRetcnt.deref(),
			// Fake null-term string
			instrDesc.toString('ascii', 0, instrDesc.indexOf(0))
		];
	}*/
	
	/*static viFindNext (findList) {
		let status;
		let instrDesc = Buffer.alloc(512);
		status = libVisa.viFindNext(findList, instrDesc);
		statusCheck(status);
		return [
			status,
			// Fake null-term string
			instrDesc.toString('ascii', 0, instrDesc.indexOf(0))
		];
	}*/
	
	/*static types.ViParseRsrc (sesn, rsrcName) {
		let status;
		let pIntfType = ref.alloc(types.ViUInt16);
		let pIntfNum = ref.alloc(types.ViUInt16);
		status = libVisa.types.ViParseRsrc(sesn, rsrcName, pIntfType, pIntfNum);
		statusCheck(status);
		return [
			status,
			// This is a VI_INTF_* define
			pIntfType.deref(),
			// This is the board #
			pIntfNum.deref()
		];
	}*/
	
	// TODO: Untested, I don't hardware that responds to this call
	/*static types.ViParseRsrcEx (sesn, rsrcName) {
		let status;
		let pIntfType = ref.alloc(types.ViUInt16);
		let pIntfNum = ref.alloc(types.ViUInt16);
		let rsrcClass = Buffer.alloc(512);
		let expandedUnaliasedName = Buffer.alloc(512);
		let aliasIfExists = Buffer.alloc(512);
		status = libVisa.types.ViParseRsrcEx(sesn, rsrcName, pIntfType, pIntfNum,
			rsrcClass, expandedUnaliasedName, aliasIfExists);
		statusCheck(status);
		return [
			status,
			// This is a VI_INTF_* define
			pIntfType.deref(),
			// This is the board #
			pIntfNum.deref(),
			rsrcClass.toString('ascii', 0, rsrcClass.indexOf(0)),
			expandedUnaliasedName.toString('ascii', 0, expandedUnaliasedName.indexOf(0)),
			aliasIfExists.toString('ascii', 0, aliasIfExists.indexOf(0))
		];
	}*/
	
	static open(rm_session, resource_name, access_mode = 0, timeout = 2000) 
	{
		const p_session = ref.alloc(types.ViSession);
		const status    = libVisa.viOpen(rm_session, resource_name, access_mode, timeout, p_session);
		
		statusCheck(status);
		return [status, p_session.deref()];
	}
	
	static close(session) 
	{
		const status = libVisa.viClose(session);

		statusCheck(status);
		return status;
	}
	
	static set_attribute(session, attribute, attribute_state)
	{
		return libVisa.viSetAttribute(session, attribute, attribute_state);
	}
	
	static read(session, max_bytes = 512) 
	{
		const read_buffer          = Buffer.alloc(max_bytes);
		const p_read_bytes_counter = ref.alloc(types.ViUInt32);

		const status = libVisa.viRead(session, read_buffer, read_buffer.length, p_read_bytes_counter);

		statusCheck(status);
		return [status, ref.reinterpret(read_buffer, p_read_bytes_counter.deref(), 0).toString()];
	}
	
	static read_raw(session, max_bytes = 512) 
	{
		const read_buffer          = Buffer.alloc(max_bytes);
		const p_read_bytes_counter = ref.alloc(types.ViUInt32);

		const status = libVisa.viRead(session, read_buffer, read_buffer.length, p_read_bytes_counter);

		statusCheck(status);
		return [status, read_buffer.slice(0, p_read_bytes_counter.deref())];
	}
	
	static read_to_file(session, file_name, number_bytes_to_read) 
	{
		const p_read_bytes_counter = ref.alloc(types.ViUInt32);
		const status = libVisa.viReadToFile(session, file_name, number_bytes_to_read, p_read_bytes_counter);

		statusCheck(status);
		return [status];
	}
	
	static write(session, buffer_data) 
	{
		const p_written_bytes_counter = ref.alloc(types.ViUInt32);
		const status = libVisa.viWrite(session, buffer_data, buffer_data.length, p_written_bytes_counter);

		statusCheck(status);
		if (p_written_bytes_counter.deref() != buffer_data.length) 
		{
			throw new Error('viWrite length fail' + `: ${p_written_bytes_counter.deref()} vs ${buffer_data.length}`)
		}
		return [status, p_written_bytes_counter.deref()];
	}
	
	/**
	 * These helper statics combine vi* statics to perform routine tasks.
	 * Error handling is left to the vi* statics.
	 */
	
	/**
	 * Returns a list of strings of found resources
	 */
	static vhListResources (sesn, expr='?*') {
		let descList = [];
		let [status, findList, retcnt, instrDesc] = viFindRsrc(sesn, expr);
		if (retcnt) {
			descList.push(instrDesc);
			for (let i = 1; i < retcnt; ++i) {
				[status, instrDesc] = viFindNext(findList);
				descList.push(instrDesc);
			}
		}
		return descList;
	}
	
	static query(session, query) 
	{
		VisaWrapper.write(session, query);
		return VisaWrapper.read(session)[1];
	}	
}

module.exports = VisaWrapper;
