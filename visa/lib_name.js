// All Electronics Channel - Gregory F. Gusberti @ 2020
// https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug

const os = require('os');

module.exports = () =>
{
	switch(os.platform()) 
	{
		case 'linux':
			return 'librsvisa';
		case 'win32':
			return os.arch() == 'x64' ? 'visa64.dll' : 'visa32.dll';
		default: 
			throw new Error('Platform not supported: ' + os.platform());
	}
}