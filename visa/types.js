// All Electronics Channel - Gregory F. Gusberti @ 2020
// https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug

const ref   = require('ref');
const types = {};

types.ViInt32       = ref.types.int32;
types.ViPInt32      = ref.refType(types.ViInt32);

types.ViUInt32      = ref.types.uint32;
types.ViPUInt32     = ref.refType(types.ViUInt32);

types.ViInt16       = ref.types.int16;
types.ViPInt16      = ref.refType(types.ViInt16);

types.ViUInt16      = ref.types.uint16;
types.ViPUInt16     = ref.refType(types.ViUInt16);

types.ViChar        = ref.types.char;
types.ViPChar       = ref.refType(types.ViChar);

types.ViByte        = ref.types.uchar;
types.ViPByte       = ref.refType(types.ViByte);

// Peter Torelli
// Note, this needs to be ViUInt32, not ViInt32 other we get negative hex
types.ViStatus      = types.ViUInt32;
types.ViObject      = types.ViUInt32;

types.ViSession     = types.ViUInt32;
types.ViPSession    = ref.refType(types.ViSession);

types.ViAttr        = types.ViUInt32;
types.ViPAttr       = ref.refType(types.ViAttr);

types.ViAttrState   = types.ViUInt32;
types.ViPAttrState  = ref.refType(types.ViAttrState);

types.ViString      = types.ViPChar;
types.ViConstString = types.ViString;

types.ViRsrc        = types.ViString;
types.ViConstRsrc   = types.ViConstString;

types.ViAccessMode  = types.ViUInt32;

types.ViBuf         = types.ViPByte;
types.ViPBuf        = types.ViPByte;

types.ViConstBuf    = types.ViPByte;

types.ViFindList    = types.ViObject;
types.ViPFindList   = ref.refType(types.ViFindList);

module.exports = types;