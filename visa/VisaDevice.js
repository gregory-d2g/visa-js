// All Electronics Channel - Gregory F. Gusberti @ 2020
// https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug

const ref         = require('ref');

const VisaWrapper = require('./VisaWrapper.js');
const constants   = require('./constants.js').constants;
const types       = require('./types.js');

let __p_default_rm_session = null;

class VisaDevice
{
	constructor(device_name)
	{
		const visa_rm = VisaDevice.__rm_single_ton();

		this.session = VisaWrapper.open(visa_rm, device_name)[1];
	}

	read(max_bytes = 512)
	{
		return VisaWrapper.read(this.session, max_bytes);
	}

	write(string)
	{
		return VisaWrapper.write(this.session, string);
	}

	query(string)
	{
		return VisaWrapper.query(this.session, string);
	}

	close()
	{
		VisaWrapper.close(this.session);
	}

	set_attribute(attribute, value)
	{
		VisaWrapper.set_attribute(this.session, attribute, value);
	}

	set_termination_char(char)
	{
		if (char === false)
		{
			this.set_attribute(constants.VI_ATTR_TERMCHAR_EN, 0);
		}
		else
		{
			this.set_attribute(constants.VI_ATTR_TERMCHAR_EN, 1);
			this.set_attribute(constants.VI_ATTR_TERMCHAR, char.charCodeAt(0));
		}
	}

	static __rm_single_ton()
	{
		if (!__p_default_rm_session)
		{
			__p_default_rm_session = VisaWrapper.open_default_rm()[1];
		}

		return __p_default_rm_session;
	}
}

module.exports = VisaDevice;