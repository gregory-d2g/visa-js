// All Electronics Channel - Gregory F. Gusberti @ 2020
// https://www.youtube.com/channel/UC0UDLi6A-auU9YetO-ShZug

function pause(timeout) 
{
	return new Promise((resolve, reject) => 
	{
		setTimeout(resolve, timeout);
	});
}

module.exports = pause;